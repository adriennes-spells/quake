provider "google" {
  project = "adrienne-devops"
  region  = "us-west1"
  zone    = "us-west1-a"
}
